# bactofidia-v1.3

This is a wrapper script to run the singularity container of bactofidia on the HPC


# Installation

In your `data` directory, clone this script

`git clone https://gitlab.com/mmb-umcu/bactofidia-v1.3.git bactofidia_v1.3`

Copy the singularity container of bactofidia to this new `bactofidia_v1.3` directory

`cp /hpc/local/Rocky8/dla_mm/tools/bactofidia-v1.2.1.sif bactofidia_v1.3/`

Change into the `bactofidia_v1.3` directory.

Copy or symlink your .fastq.gz files to this directory.

Make a screen session and start an interactive session.

Run with 

`./bactofidia-v1.3 ALL`

For further information, see [bactofidia README](https://gitlab.com/aschuerch/bactofidia/-/blob/master/README.md)

