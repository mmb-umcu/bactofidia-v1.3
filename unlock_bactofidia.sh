#!/bin/bash -i


#remove all generated temporary directories if any
for i in tmp data stats results log
 do
 rm -r "$i"
 done

# command to unlock the workflow
snakemake --snakefile Snakefile.assembly  --cores all --unlock --config configfile=config/config.yaml
